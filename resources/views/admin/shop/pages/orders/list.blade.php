@extends('admin.index')
@section('styles')
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Cписок заказов</h5>
                @if(request()->has('trashed'))
                    <a href="{{route('orders.index', ['type'=> request()->input('type')])}}" class="ml-5">К
                        опубликованному</a>
                @else
                    <a href="{{route('orders.index', ['type'=> request()->input('type')])}}&trashed" class="ml-5">В
                        корзину</a>
                @endif
            </div>
            @component('admin.components.breadcrumbs')
                @slot('active') {{$active}} @endslot
            @endcomponent
        </div>
        <!-- /Title -->

        <div class="row">

            <!-- Bordered Table -->
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <form action="" method="get">
                                    <div class="row">
                                        <div class="col-xs-6 pl-0">
                                            <select name="status" class="form-control">
                                                <option value="">Все</option>
                                                @foreach(\App\Models\Shop\Order::STATUSES as $status => $name)
                                                    @if($status != 'cart')
                                                    <option value="{{$status}}"
                                                            @if(request()->status == $status) selected @endif>{{$name}} </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-xs-2">
                                            <button class="btn btn-primary" type="submit">Показать</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>Номер</th>
                                            <th>Клиент</th>
                                            <th>Содержимое заказа</th>
                                            <th>Информация о заказе</th>
                                            <th class="text-nowrap">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $order )
                                            <tr class="js-order-row" data-id="{{$order->id}}">
                                                <td>
                                                   {{$order->number}}
                                                </td>
                                                <td> {{$order->client}}</td>
                                                <td>
                                                    <small>{{$order->getReadableContent()}}</small>
                                                </td>
                                                <td>
                                                    Создан: {{date("H:i d.m.Y", strtotime($order->created_at))}}<br>
                                                    @if(!empty($order->time))Доставить к: {{$order->time}} | @endif
                                                    Оплата: {{\App\Models\Shop\Order::PAYMENTS[$order->payment]}}<br>
                                                    @if(!empty($order->phone) || !empty($order->email))
                                                    Контакты:
                                                        @if(!empty($order->phone))
                                                        {{$order->phone}}
                                                        @endif
                                                        @if(!empty($order->email))
                                                        |
                                                        {{$order->email}}
                                                        @endif
                                                        <br>
                                                    @endif
                                                    @if(!empty($order->address)) Адрес: {{$order->address}}<br>@endif
                                                    Персон: {{$order->persons}}
                                                </td>
                                                <td class="text-nowrap">
                                                    {{-- <a href="" class="mr-10"
                                                        data-toggle="tooltip" data-original-title="Редактировать"> <i
                                                                 class="fa fa-pencil text-inverse"></i> </a>--}}
                                                    <div>
                                                        Статус:
                                                        <select class="js-order-status">
                                                            @foreach(\App\Models\Shop\Order::STATUSES as $status => $name)
                                                                @if($status != 'cart')
                                                                    <option value="{{$status}}"
                                                                    @if($order->status == $status) selected @endif
                                                                        >{{$name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>&nbsp;
                                                        <a href="#" data-toggle="tooltip" class="mr-10"
                                                           data-original-title="Удалить заказ"> <i
                                                                    class="fa fa-close btn-modal-close text-danger"></i>
                                                        </a>
                                                    </div>

                                                    @if(!is_null($order->deleted_at))
                                                        <a href="#" data-toggle="tooltip" class="mr-10"
                                                           data-original-title="Восстановить"> <i
                                                                    class="fa fa-arrow-up btn-modal-restore text-success"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Bordered Table -->

        </div>

    </div>
@endsection

@component('admin.components.modal_delete')
    @slot('modal_text') {{!request()->has('trashed') ? 'убрать заказ в корзину': 'окончательно удалить заказ'}}  @endslot
    @slot('modal_caption') {{!request()->has('trashed') ? 'Убрать': 'Удалить'}} @endslot
@endcomponent

@if(request()->has('trashed'))
    @component('admin.components.modal_restore')
        @slot('modal_target') заказ @endslot
    @endcomponent
@endif
@section('scripts')
    <script>
        $(".btn-modal-close").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#delModal").modal();
            $(".btn-del").click(function () {
                window.location.href = "/admin/orders/" + id + "/delete  ";
            });
        });

        $(".btn-modal-restore").click(function () {
            var id = $(this).closest('tr').data('id');
            console.log(id);
            $("#restoreModal").modal();
            $(".btn-rest").click(function () {
                window.location.href = "/admin/orders/" + id + "/restore";
            });
        });

        $('.js-order-status').change(function () {
            var id = $(this).closest('.js-order-row').data('id');
            var status = $(this).val();
            $.post("{{route('orders.update.status')}}", {_token:"{{csrf_token()}}", id: id, status: status }, function (data) {

            });

        });



    </script>


@endsection