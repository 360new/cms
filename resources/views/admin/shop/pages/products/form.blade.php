@extends('admin.index')
@section('styles')
    <!-- Summernote css -->
    <link rel="stylesheet" href="/admin-files/vendors/bower_components/summernote/dist/summernote.css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Редактирование товара</h5>
            </div>

            @component('admin.components.breadcrumbs')
                @slot('parent_route') {{route('products.index')}} @endslot
                @slot('parent') Товары @endslot
                @slot('active') {{$active}} @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapseProduct" aria-expanded="true"
                                   aria-controls="collapseProduct" class="d-block">
                                    <h6 class="panel-title txt-dark">Редактор</h6>
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in" id="collapseProduct">
                                <div class="panel-body">
                                    <div class="form-wrap mt-10">
                                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                                              action="{{route('products.update', $product->id)}}"
                                              method="POST">

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Название</label>
                                                <input type="text" class="form-control" value="{{$product->name}}"
                                                       name="name"
                                                       placeholder="Новый товар">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Ссылка</label>
                                                <input type="text" class="form-control" value="{{$product->slug}}"
                                                       name="slug"
                                                       placeholder="new_product">
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-6">
                                                    <label class="control-label mb-10 text-left">Артикул</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$product->article}}"
                                                           name="article"
                                                           placeholder="000000">
                                                </div>
                                                <div class="form-group col-xs-2">

                                                    <label class="control-label mb-10 text-left">В топе
                                                        <input type="hidden" name="top" value="0">
                                                        <input type="checkbox" class="form-control" name="top" value="1"
                                                               @if($product->top)
                                                               checked
                                                                @endif >
                                                    </label>
                                                </div>
                                                <div class="form-group col-xs-2">

                                                    <label class="control-label mb-10 text-left">В стоп-листе
                                                        <input type="hidden" name="sold" value="0">
                                                        <input type="checkbox" class="form-control" name="sold"
                                                               value="1"
                                                               @if($product->sold)
                                                               checked
                                                                @endif >
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-4">
                                                    <label class="control-label mb-10 text-left">Цена</label>
                                                    <input type="number" step="0.01" class="form-control"
                                                           value="{{$product->price}}"
                                                           name="price"
                                                           min="0">
                                                </div>
                                                <div class="form-group col-xs-4">
                                                    <label class="control-label mb-10 text-left">Вес</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$product->weight}}"
                                                           name="weight">
                                                </div>
                                                <div class="form-group col-xs-4">
                                                    <label class="control-label mb-10 text-left">Минимум к заказу <i
                                                                class="fa fa-info-circle"
                                                                title="Минимальное количество товара, которое пользователь может добавить в корзину"></i>
                                                    </label>
                                                    <input type="number" class="form-control"
                                                           value="{{$product->minimum}}"
                                                           name="minimum" min="0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Краткое описание</label>
                                                <textarea class="summernote"
                                                          name="short_product_description">{!!$product->short_product_description!!}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Дополнительное
                                                    описание</label>
                                                <textarea class="summernote1"
                                                          name="additional_product_description">{!! $product->additional_product_description !!}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Описание</label>
                                                <textarea class="summernote2"
                                                          name="product_description">{!! $product->product_description!!}</textarea>
                                            </div>
                                            <input type="hidden" name="id" value="{{$product->id}}">
                                            <input type="hidden" name="user_id" value="{{$product->user_id}}">
                                            <input type="hidden" name="form_type" value="product">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @component('admin.components.panels.gallery_panel',
                    ['gallery' => $product->gallery(), 'id' => $product->id])
                    @endcomponent

                    @component('admin.components.panels.seo_panel')
                        @slot('keywords') {{$product->keywords}} @endslot
                        @slot('description') {{$product->descripion}} @endslot
                    @endcomponent
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    @component('admin.components.blocks.control_block',
                    ['deleted_at' => $product->deleted_at])
                        @slot('created_at') {{$product->created_at}} @endslot
                        @slot('status') {{$product->status}} @endslot
                    @endcomponent

                    @component('admin.components.blocks.miniature_block',
                    ['miniature' => $product->miniature(), 'id' => $product->id])
                    @endcomponent

                    @component('admin.components.blocks.categories_block',
                     ['object' => $product, 'object_categories' => $product_categories])
                    @endcomponent

                    @component('admin.components.blocks.tags_block',
                       ['object' => $product, 'object_tags' => $product_tags])
                    @endcomponent

                </div>
            </div>

            <!-- /Row -->


        </div>
        @endsection

        @component('admin.components.modal_delete')
            @slot('modal_text') {{is_null($product->deleted_at)? 'убрать товар в корзину': 'окончательно удалить товар'}}  @endslot
            @slot('modal_caption') {{is_null($product->deleted_at)? 'Убрать': 'Удалить'}} @endslot
        @endcomponent
        @if(!is_null($product->deleted_at))
            @component('admin.components.modal_restore')
                @slot('modal_target') товар @endslot
            @endcomponent
        @endif
        @section('scripts')
            <script>$(".chosen-select").chosen({
                    width: "100%",
                });</script>
            <!-- Summernote Plugin JavaScript -->
            <script src="/admin-files/vendors/bower_components/summernote/dist/summernote.min.js"></script>
            <script src="/admin-files/vendors/bower_components/summernote/lang/summernote-ru-RU.js"></script>
            <script>
                $('.summernote').summernote({
                    lang: 'ru-RU',
                    height: 200,
                    callbacks: {
                        onImageUpload: function (files) {
                            var el = $(this);
                            sendFile(files[0], el);
                        }
                    }
                });

                $('.summernote1').summernote({
                    lang: 'ru-RU',
                    height: 300,
                    callbacks: {
                        onImageUpload: function (files) {
                            var el = $(this);
                            sendFile(files[0], el);
                        }
                    }
                });

                $('.summernote2').summernote({
                    lang: 'ru-RU',
                    height: 300,
                    callbacks: {
                        onImageUpload: function (files) {
                            var el = $(this);
                            sendFile(files[0], el);
                        }
                    }
                });

                function sendFile(file, el) {
                    var data = new FormData();
                    data.append("image", file);
                    data.append("type", 'content_image');
                    data.append("parent_type", 'product');
                    data.append("parent_id", $('input[name="id"]').val());
                    var url = '{{ route('image.upload') }}';
                    $.ajax({
                        data: data,
                        type: "POST",
                        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                        url: url,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (link) {
                            el.summernote('insertImage', link);
                        }
                    });
                }


                <!-- End Summernote-->

                $(".btn-del").click(function () {
                    window.location.href = "/admin/products/" + $('input[name="id"]').val() + "/delete";
                });
                $(".btn-rest").click(function () {
                    window.location.href = "/admin/products/" + $('input[name="id"]').val() + "/restore";
                });
                $('input[name="name"]').blur(function () {
                    if ($('input[name="slug"]').val().length == 0) {
                        $.get("{{route('generate.slug')}}", {
                            _token: "{{csrf_token()}}",
                            slug_string: $('input[name="name"]').val()
                        }, function (data) {
                            $('input[name="slug"]').val(data);
                        });
                    }

                    if ($('input[name="id"]').val() === "0") {

                        $.post("{{route('products.store')}}", {
                            _token: "{{csrf_token()}}",
                            name: $('input[name="name"]').val()
                        }, function (data) {
                            $('input[name="id"]').val(data);
                            $('input[name="gallery\\[\\]"]').prop("disabled", false);
                            $('input[name="image"]').prop("disabled", false);
                            $('#form-data').attr("action", "/admin/products/" + data);

                        });
                    }

                });
            </script>

@endsection