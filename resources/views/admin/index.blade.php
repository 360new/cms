<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>CMS{{isset($title) ? $title:''}}</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Data table CSS -->
    <link href="/admin-files/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet"
          type="text/css"/>

    <link href="/admin-files/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet"
          type="text/css">
    <link href="/admin-files/vendors/chosen/chosen.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="/admin-files/dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="/admin-files/dist/css/custom.css" rel="stylesheet" type="text/css">
    @yield('styles')
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-green">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="/">
                        {{--<img class="brand-img" src="/admin-files/dist/img/logo.png" alt="brand"/>--}}
                        <span class="h6">CMS</span>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left"
               href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            {{--<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>--}}
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
            {{--<form id="search_form" role="search" class="top-nav-search collapse pull-left">
                <div class="input-group">
                    <input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                        <button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
                        </span>
                </div>
            </form>--}}
        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">
                <li class="dropdown auth-drp">
                    <a href="#" class="dropdown-toggle pr-0"
                       data-toggle="dropdown"><span>{{Auth::user()->name}}</span><span
                                class="user-online-status"></span></a>
                    <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX"
                        data-dropdown-out="flipOutX">
                        <li>
                            <a href="{{route('users.edit', Auth::user()->id)}}"><i class="zmdi zmdi-settings"></i><span>Настройки</span></a>
                        </li>
                        <li>
                            <a href="{{route('logout.get')}}"><i class="zmdi zmdi-power"></i><span>Выйти</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">
            <li class="navigation-header">
                <span>Магазин</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a href="{{route('orders.index')}}">
                    <div class="pull-left"><i class="zmdi zmdi-shopping-basket mr-20"></i><span class="right-nav-text">Заказы</span>
                    </div>
                    <div class="pull-right"></div>
                    <div class="clearfix"></div>
                </a>
            </li>
            {{--<li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#orders_dr">
                    <div class="pull-left"><i class="zmdi zmdi-shopping-basket mr-20"></i><span
                                class="right-nav-text">Заказы</span></div>
                    <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                    <div class="clearfix"></div>
                </a>
                <ul id="orders_dr" class="collapse collapse-level-1">
                    <li>
                        <a href="{{route('orders.create')}}">Добавить</a>
                    </li>
                    <li>
                        <a href="{{route('orders.index')}}">Все</a>
                    </li>
                </ul>

            </li>--}}

            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#prod_cat_dr">
                    <div class="pull-left"><i class="zmdi zmdi-collection-bookmark mr-20"></i><span
                                class="right-nav-text">Категории товаров</span></div>
                    <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                    <div class="clearfix"></div>
                </a>
                <ul id="prod_cat_dr" class="collapse collapse-level-1">
                    <li>
                        <a href="{{route('product_categories.create', ['type' => 'category'])}}">Добавить</a>
                    </li>
                    <li>
                        <a href="{{route('product_categories.index', ['type' => 'category'])}}">Все</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#prod_tagdr">
                    <div class="pull-left"><i class="zmdi zmdi-view-subtitles mr-20"></i><span class="right-nav-text">Теги</span>
                    </div>
                    <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                    <div class="clearfix"></div>
                </a>
                <ul id="prod_tagdr" class="collapse collapse-level-1">
                    <li>
                        <a href="{{route('product_categories.create',['type' => 'tag'])}}">Добавить</a>
                    </li>
                    <li>
                        <a href="{{route('product_categories.index',['type' => 'tag'])}}">Все</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#prod_dr">
                    <div class="pull-left"><i class="zmdi zmdi-cutlery mr-20"></i><span
                                class="right-nav-text">Товары</span></div>
                    <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                    <div class="clearfix"></div>
                </a>
                <ul id="prod_dr" class="collapse collapse-level-1">
                    <li>
                        <a href="{{route('products.create')}}">Добавить</a>
                    </li>
                    <li>
                        <a href="{{route('products.index')}}">Все</a>
                    </li>
                </ul>
            </li>
            @if( Auth::user()->role->role != 'shop_manager')
                <li class="navigation-header">
                    <span>Меню</span>
                    <i class="zmdi zmdi-more"></i>
                </li>

                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#cat_dr">
                        <div class="pull-left"><i class="zmdi zmdi-labels mr-20"></i><span class="right-nav-text">Категории</span>
                        </div>
                        <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                        <div class="clearfix"></div>
                    </a>
                    <ul id="cat_dr" class="collapse collapse-level-1">
                        <li>
                            <a href="{{route('categories.create',['type' => 'category'])}}">Добавить</a>
                        </li>
                        <li>
                            <a href="{{route('categories.index',['type' => 'category'])}}">Все</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#tagdr">
                        <div class="pull-left"><i class="zmdi zmdi-view-subtitles mr-20"></i><span
                                    class="right-nav-text">Теги</span></div>
                        <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                        <div class="clearfix"></div>
                    </a>
                    <ul id="tagdr" class="collapse collapse-level-1">
                        <li>
                            <a href="{{route('categories.create',['type' => 'tag'])}}">Добавить</a>
                        </li>
                        <li>
                            <a href="{{route('categories.index',['type' => 'tag'])}}">Все</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr">
                        <div class="pull-left"><i class="zmdi zmdi-copy mr-20"></i><span
                                    class="right-nav-text">Страницы</span></div>
                        <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                        <div class="clearfix"></div>
                    </a>
                    <ul id="pages_dr" class="collapse collapse-level-1">
                        <li>
                            <a href="{{route('posts.create', ['type' => 'page'])}}">Добавить</a>
                        </li>
                        <li>
                            <a href="{{route('posts.index',  ['type' => 'page'])}}">Все</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#news_dr">
                        <div class="pull-left"><i class="zmdi zmdi-receipt mr-20"></i><span class="right-nav-text">Новости</span>
                        </div>
                        <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                        <div class="clearfix"></div>
                    </a>
                    <ul id="news_dr" class="collapse collapse-level-1">
                        <li>
                            <a href="{{route('posts.create',  ['type' => 'news'])}}">Добавить</a>
                        </li>
                        <li>
                            <a href="{{route('posts.index',  ['type' => 'news'])}}">Все</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#users_dr">
                        <div class="pull-left"><i class="zmdi zmdi-accounts mr-20"></i><span class="right-nav-text">Пользователи</span>
                        </div>
                        <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                        <div class="clearfix"></div>
                    </a>
                    <ul id="users_dr" class="collapse collapse-level-1">
                        <li>
                            <a href="{{route('users.create')}}">Добавить</a>
                        </li>
                        <li>
                            <a href="{{route('users.index')}}">Все</a>
                        </li>
                    </ul>
                </li>
                {{--<li>
                    <a href="{{route('menus.index')}}">
                        <div class="pull-left"><i class="zmdi zmdi-dns mr-20"></i><span
                                    class="right-nav-text">Меню</span></div>
                        <div class="pull-right"></div>
                        <div class="clearfix"></div>
                    </a>
                </li>--}}
                <li>
                    <a href="{{route('settings.index')}}">
                        <div class="pull-left"><i class="zmdi zmdi-settings mr-20"></i><span class="right-nav-text">Настройки</span>
                        </div>
                        <div class="pull-right"></div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li>
                    <hr class="light-grey-hr mb-10"/>
                </li>
            @endif
        </ul>
    </div>
    <!-- /Left Sidebar Menu -->
    <!-- Main Content -->
    <div class="page-wrapper">
    @yield('content')
    <!-- /Main Content -->
        <!-- Footer -->
        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p><a href="http://360design.ru" target="_blank">360 Studio</a></p>
                </div>
            </div>
        </footer>
        <!-- /Footer -->

    </div>

</div>
<!-- /#wrapper -->
@yield('modals')

<!-- JavaScript -->

<!-- jQuery -->
<script src="/admin-files/vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/admin-files/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Data table JavaScript -->
<script src="/admin-files/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="/admin-files/dist/js/jquery.slimscroll.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="/admin-files/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="/admin-files/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="/admin-files/dist/js/dropdown-bootstrap-extended.js"></script>

<!-- Sparkline JavaScript -->
<script src="/admin-files/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="/admin-files/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- Switchery JavaScript -->
<script src="/admin-files/vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="/admin-files/dist/js/init.js"></script>
<script src="/admin-files/vendors/chosen/chosen.jquery.min.js"></script>

@yield('scripts')

</body>

</html>
