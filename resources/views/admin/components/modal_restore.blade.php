<div id="restoreModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="restoreModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="restoreModalLabel">Подтвердите восстановление</h5>
            </div>
            <div class="modal-body">
                <p>Вы действительно хотите восстановить {{$modal_target}}?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-rest pull-left"
                        data-dismiss="modal">Восстановить</button>
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Отмена</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>