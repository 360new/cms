<!-- Breadcrumb -->
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <ol class="breadcrumb">
        <li><a href="/admin">Главная</a></li>
        @if(isset($parent_route))
            <li><a href="{{$parent_route}}"><span>{{$parent}}</span></a></li>
        @endif
        <li class="active"><span> {{$active}} </span></li>
    </ol>
</div>
<!-- /Breadcrumb -->
