<div class="col-md-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <a role="button" data-toggle="collapse" href="#collapseCategories" aria-expanded="true"
               aria-controls="collapseCategories" class="d-block">
                <h6 class="panel-title txt-dark">Теги</h6>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in" id="collapseCategories">
            <div class="panel-body">
                <div class="form-wrap">
                    <select data-placeholder="Выберите тег" class="chosen-select" name="categories[]" multiple form="form-data">
                        @foreach($object_tags as $tag)
                            <option value="{{$tag->id}}"
                                    @if($object->categories->contains($tag->id))
                                    selected
                                    @endif>{{$tag->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>