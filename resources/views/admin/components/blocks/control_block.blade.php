<div class="col-md-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <a role="button" data-toggle="collapse" href="#collapseControl" aria-expanded="true"
               aria-controls="collapseControl" class="d-block">
                <h6 class="txt-dark"> Управление</h6>
            </a>
        </div>
        <div class="panel-wrapper collapse in mt-10" id="collapseControl">
            <div class="panel-body">
                <p>Создано: {{$created_at}}</p>
                <p>Статус:
                    @if(!is_null($deleted_at))
                    В корзине
                    @else
                    <select name="status" id="" form="form-data">
                        <option value="public" @if($status == 'public')selected @endif>Опубликовано</option>
                        <option value="hidden" @if($status == 'hidden')selected @endif>В черновиках</option>
                    </select>
                    @endif
                </p>
                <div class="form-wrap mt-10">
                    <button class="btn btn-primary pull-left" type="submit" form="form-data">
                        Сохранить
                    </button>
                    @if(!is_null($deleted_at))
                        <a class="pull-right pointer" data-toggle="modal"
                           data-target="#restoreModal">Восстановить</a>
                    @endif
                    @if(!str_contains(Route::currentRouteName(), 'create'))
                    <a class="pull-right pointer" data-toggle="modal" data-target="#delModal">Удалить</a>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>