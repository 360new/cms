<div class="col-md-12">
    <div class="panel panel-default card-view">
        <div class="panel-heading">
            <a role="button" data-toggle="collapse" href="#collapseMiniature" aria-expanded="true"
               aria-controls="collapseMiniature" class="d-block">
                <h6 class="panel-title txt-dark">Миниатюра</h6>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in" id="collapseMiniature">
            <div class="panel-body">
                <div class="form-wrap">
                    @if(is_object($miniature))
                        <img src="{{$miniature->small}}" alt="" class="image-responsive mb-15" width="100%">
                    @endif
                    <input type="file" name="image" form="form-data" @if($id == 0) disabled @endif>
                    @if(is_object($miniature))
                        <label for="">Удалить миниатюру
                            <input type="checkbox" name="img_delete[]"
                                   form="form-data" value="{{$miniature->id}}">
                        </label>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>