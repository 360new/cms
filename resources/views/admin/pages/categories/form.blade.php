@extends('admin.index')
@section('styles')
    <!-- Summernote css -->
    <link rel="stylesheet" href="/admin-files/vendors/bower_components/summernote/dist/summernote.css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Редактирование категории</h5>
            </div>

            @component('admin.components.breadcrumbs')
                @slot('parent_route') {{route('categories.index', ['type' => $category->type])}} @endslot
                @slot('parent') Категории @endslot
                @slot('active') {{$active}} @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapseCategory" aria-expanded="true"
                                   aria-controls="collapseCategory" class="d-block">
                                    <h6 class="panel-title txt-dark">Редактор</h6>
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in" id="collapseCategory">
                                <div class="panel-body">
                                    <div class="form-wrap mt-10">
                                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                                              action="{{route('categories.update', $category->id)}}"
                                              method="POST">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Название</label>
                                                <input type="text" class="form-control" value="{{$category->name}}"
                                                       name="name"
                                                       placeholder="Новая категория">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Ссылка</label>
                                                <input type="text" class="form-control" value="{{$category->slug}}"
                                                       name="slug"
                                                       placeholder="new_post">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Описание</label>
                                                <textarea name="caption" rows="10"
                                                          class="form-control no-resize summernote">{{$category->caption}}</textarea>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-xs-6">
                                                    <label class="control-label mb-10 text-left">Тип</label>
                                                    <select name="type" id="" class="cat-type">
                                                        <option value="category"
                                                                @if($category->type == 'category') selected @endif>
                                                            Категория
                                                        </option>
                                                        <option value="tag"
                                                                @if($category->type == 'tag') selected @endif>
                                                            Тег
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="col-xs-6 parent-selector">
                                                    <label class="control-label mb-10 text-left">Родитель</label>
                                                    <select name="parent_id" id="">
                                                        <option value=""
                                                                @if(is_null($category->parent_id)) selected @endif>
                                                            Без
                                                            родителя
                                                        </option>

                                                        @foreach($categories as $cat)
                                                            <option value="{{$cat->id}}"
                                                                    @if($category->parent_id == $cat->id) selected @endif>{{$cat->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id" value="{{$category->id}}">
                                            <input type="hidden" name="user_id" value="{{$category->user_id}}">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @component('admin.components.panels.gallery_panel',
                    ['gallery' => $category->gallery(), 'id' => $category->id])
                    @endcomponent

                    @component('admin.components.panels.seo_panel')
                        @slot('keywords') {{$category->keywords}} @endslot
                        @slot('description') {{$category->description}} @endslot
                    @endcomponent
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    @component('admin.components.blocks.control_block',
                    ['deleted_at' => $category->deleted_at])
                        @slot('created_at') {{$category->created_at}} @endslot
                        @slot('status') {{$category->status}} @endslot
                    @endcomponent

                    @component('admin.components.blocks.miniature_block',
                    ['miniature' => $category->miniature(), 'id' => $category->id])
                    @endcomponent

                </div>
            </div>

            <!-- /Row -->


        </div>
        <!-- /Row -->


    </div>
@endsection

@component('admin.components.modal_delete')
    @slot('modal_text') {{is_null($category->deleted_at)? 'убрать категорию в корзину': 'окончательно удалить категорию'}}  @endslot
    @slot('modal_caption') {{is_null($category->deleted_at)? 'Убрать': 'Удалить'}} @endslot
@endcomponent
@if(!is_null($category->deleted_at))
    @component('admin.components.modal_restore')
        @slot('modal_target') категорию @endslot
    @endcomponent
@endif
@section('scripts')
    <script src="/admin-files/vendors/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="/admin-files/vendors/bower_components/summernote/lang/summernote-ru-RU.js"></script>
    <script>
        <!-- Summernote Plugin JavaScript -->
        $('.summernote').summernote({
            lang: 'ru-RU',
            height: 300,
            callbacks: {
                onImageUpload: function(files) {
                    var el = $(this);
                    sendFile(files[0],el);
                }
            }
        });

        function sendFile(file, el) {
            var  data = new FormData();
            data.append("image", file);
            data.append("type", 'content_image');
            data.append("parent_type", 'category');
            data.append("parent_id",  $('input[name="id"]').val());
            var url = '{{ route('image.upload') }}';
            $.ajax({
                data: data,
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                success: function(link) {
                    el.summernote('insertImage', link);
                }
            });
        }

        $(".btn-del").click(function () {
            window.location.href = "/admin/categories/" + $('input[name="id"]').val() + "/delete";
        });
        $(".btn-rest").click(function () {
            window.location.href = "/admin/categories/" + $('input[name="id"]').val() + "/restore";
        });
        $('input[name="name"]').blur(function(){
            if($('input[name="slug"]').val().length == 0) {
                $.get("{{route('generate.slug')}}", {_token:"{{csrf_token()}}", slug_string: $('input[name="name"]').val()}, function (data) {
                    $('input[name="slug"]').val(data);
                });
            }

            if ($('input[name="id"]').val() === "0") {

                $.post("{{route('categories.store')}}", {
                    _token: "{{csrf_token()}}",
                    name: $('input[name="name"]').val(),
                    type: $('select[name="type"]').val()
                }, function (data) {
                    $('input[name="id"]').val(data);
                    $('input[name="gallery\\[\\]"]').prop("disabled", false);
                    $('input[name="image"]').prop("disabled", false);
                    $('#form-data').attr("action", "/admin/categories/" + data);

                });
            }
        });
    </script>

@endsection