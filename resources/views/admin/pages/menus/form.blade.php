@extends('admin.index')

@section('styles')
    <!--Nestable CSS -->
    <link href="/admin-files/vendors/bower_components/nestable2/jquery.nestable.css" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Управление меню</h5>
            </div>

            @component('admin.components.breadcrumbs')
                @slot('active') Меню @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapseControl" aria-expanded="true"
                                   aria-controls="collapseControl" class="d-block">
                                    <h6 class="txt-dark"> Управление</h6>
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in mt-10" id="collapseControl">
                                <div class="panel-body">
                                    <table class="table table-bordered menus_list">
                                        <tr>
                                            <td><a href="{{route('menus.index',['edit'=> 0])}}" class="btn btn-default w-100">Создать
                                                    меню</a>
                                            </td>
                                        </tr>
                                        @foreach($zones as $zone)
                                            <tr>
                                                <td><a href="{{route('menus.index',['edit'=> $zone->id])}}" class="btn btn-primary w-100">{{$zone->name}}</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                              action="{{route('menus.update', $cur_zone->id)}}"
                              method="POST">
                            <input type="hidden">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="text"
                                                   value="{{$cur_zone->name}}"
                                                   class="form-control" name="name">
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-default">Добавить элемент</button>
                                            <button class="btn btn-primary">Сохранить</button>
                                            <button class="btn btn-danger">Удалить</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-wrapper collapse in" id="collapsePost">
                                    <div class="panel-body">


                                            <div class="dd" id="nestable">
                                                {!! buildMenuItemsTree($cur_zone->baseItems()) !!}
                                            </div>
                                            <input type="hidden" name="id" value="{$post->id}}">
                                            <input type="hidden" name="user_id" value="{$post->user_id}}">
                                            <input type="hidden" name="type" value="{$post->type}}">
                                            @csrf

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /Row -->
        </div>
    @endsection
    @section('scripts')
        <!--Nestable js -->
            <script src="/admin-files/vendors/bower_components/nestable2/jquery.nestable.js"></script>
            <!-- Nestable Init JavaScript -->
            <script>
                /*Nestable Init*/
                $(document).ready(function () {
                    "use strict";

                    /*Nestable*/

                    $('#nestable').nestable({group: 1});
                });
            </script>
@endsection