@extends('admin.index')
@section('styles')
    <!-- Summernote css -->
    <link rel="stylesheet" href="/admin-files/vendors/bower_components/summernote/dist/summernote.css"/>
@endsection
@section('content')

    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Редактирование пользователя</h5>
            </div>
            <!-- TODO: Trello -REFRACT FOR USING POLICIES -->
            @component('admin.components.breadcrumbs')
                @if(!in_array(Auth::user()->role->role,['user', 'shop_manager']))
                    @slot('parent_route') {{route('users.index')}} @endslot
                    @slot('parent') Пользователи @endslot
                @endif
                @slot('active') {{$active}} @endslot
            @endcomponent

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <a role="button" data-toggle="collapse" href="#collapsePost" aria-expanded="true"
                                   aria-controls="collapsePost" class="d-block">
                                    <h6 class="panel-title txt-dark">Редактор</h6>
                                </a>
                            </div>
                            <div class="panel-wrapper collapse in" id="collapsePost">
                                <div class="panel-body">
                                    <div class="form-wrap mt-10">
                                        <form name="form-data" id="form-data" enctype="multipart/form-data"
                                              action="{{route('users.update', $user->id)}}"
                                              method="POST">
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Имя пользователя</label>
                                                <input type="text" class="form-control" value="{{$user->name}}"
                                                       name="name"
                                                       placeholder="Новый пользователь">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Email</label>
                                                <input type="email" class="form-control" value="{{$user->email}}"
                                                       name="email"
                                                       placeholder="user@example.com">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Пароль</label>
                                                <input type="password" class="form-control"
                                                       name="password">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Подтверждение
                                                    пароля</label>
                                                <input type="password" class="form-control"
                                                       name="password_repeat">
                                            </div>

                                            <!-- TODO: Trello - REFRACT FOR USING POLICIES -->
                                            @if(!in_array(Auth::user()->role->role,['user', 'shop_manager']))
                                                <div class="form-group">
                                                    <select name="role_id" id="">
                                                        @foreach($roles as $role)
                                                            <option value="{{$role->id}}"
                                                                    @if($user->role_id == $role->id) selected @endif>{{$role->role_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif
                                            <input type="hidden" name="id" value="{{$user->id}}">
                                            @method('PUT')
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    @component('admin.components.blocks.control_block',
                    ['deleted_at' => $user->deleted_at])
                        @slot('created_at') {{$user->created_at}} @endslot
                        @slot('status') '' @endslot
                    @endcomponent

                    @component('admin.components.blocks.miniature_block',
                    ['miniature' => $user->miniature(), 'id' => $user->id])
                    @endcomponent
                </div>
            </div>

            <!-- /Row -->


        </div>
        @endsection

        @component('admin.components.modal_delete')
            @slot('modal_text') {{is_null($user->deleted_at)? 'убрать пользователя в корзину': 'окончательно удалить пользователя'}}  @endslot
            @slot('modal_caption') {{is_null($user->deleted_at)? 'Убрать': 'Удалить'}} @endslot
        @endcomponent
        @if(!is_null($user->deleted_at))
            @component('admin.components.modal_restore')
                @slot('modal_target') пользователя @endslot
            @endcomponent
        @endif
        @section('scripts')
            <script>
                $(".btn-del").click(function () {
                    window.location.href = "/admin/users/" + $('input[name="id"]').val() + "/delete";
                });
                $(".btn-rest").click(function () {
                    window.location.href = "/admin/users/" + $('input[name="id"]').val() + "/restore";
                });

                $('input[name="name"]').blur(function () {
                    if ($('input[name="id"]').val() === "0") {

                        $.post("{{route('users.store')}}", {
                            _token: "{{csrf_token()}}",
                            name: $('input[name="name"]').val(),

                        }, function (data) {
                            $('input[name="id"]').val(data);
                            $('input[name="image"]').prop("disabled", false);
                            $('#form-data').attr("action", "/admin/users/" + data);
                        });
                    }
                });
            </script>

@endsection