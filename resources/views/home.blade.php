@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Успешно</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Вы успешно вошли на сайт!</p> <a href="/">Вернуться на главную страницу</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
