<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('trashed')) {
            $users = User::onlyTrashed();
        } else {
            $users = User::whereNotNull('role_id');
        }

        if (isset($request->role_id)) {
            $users->where('role_id', $request->role_id);
        }


        return view('admin.pages.users.list', [
            'users' => $users->get(),
            'title'  => ' | Список пользователей',
            'active' => 'Пользователи'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $user->id = 0;
        return view('admin.pages.users.form', [
            'user' => $user,
            'roles' => Role::all(),
            'title'  => ' | Создание пользователя',
            'active' => 'Новый пользователь'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->save();
        return Response($user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();

        return view('admin.pages.users.form', [
            'user' => $user,
            'roles' => Role::all(),
            'title' => '| Редактирование пользователя | '. $user->name,
            'active' => $user->name ? $user->name : 'Новый пользователь'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::withTrashed()->where('id', $id)->first();

        if(!is_null($request->password) && $request->password == $request->password_repeat) {
            $user->password = bcrypt($request->password);
            unset($request['password']);
        } else {
            unset($request['password']);
        }
        $user->fill($request->all());


        if(isset($request->image)){
            if($user->miniature()) {
                ImageController::deleteImage($user->miniature()->id);
            }
            ImageController::uploadImage('miniature', 'user', $id, $request->image);
        }
        $user->save();

        return Redirect::back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();

        if ($user->trashed()) {
            $user->forceDelete();
            return redirect(route('users.index'));
        } else {
            $user->delete();
            return Redirect::back();
        }
    }

    /**
     * Restore deleted post.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();
        $user->restore();
        return Redirect::back();
    }
}
