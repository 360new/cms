<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;

use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\In;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('trashed')) {
            $posts = Post::onlyTrashed();
        } else {
            $posts = Post::whereNotNull('user_id');
        }

        $posts->where('type', $request->type);

        if (isset($request->status)) {
            $posts->where('status', $request->status);
        }

        if (isset($request->user_id)) {
            $posts->where('user_id', $request->user_id);
        }


        return view('admin.pages.posts.list', [
            'posts' => $posts->get(),
            'title' => ' | Список записей',
            'active' => 'Записи'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request['user_id'] = Auth::user()->id;
        $post = new Post($request->all());
        $post->id = 0;
        return view('admin.pages.posts.form', [
            'post' => $post,
            'title' => ' | Создание записи',
            'categories' => Category::where('type', 'category')->where('parent_id', null)->get(),
            'tags' => Category::where('type', 'tag')->get(),
            'active' => 'Новая запись'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->title;
        $post->type = $request->type;
        $post->user_id = Auth::user()->id;
        $post->save();
        return Response($post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();

        return view('admin.pages.posts.form', [
            'post' => $post,
            'title' => ' | Редактирование записи | '. $post->title,
            'categories' => Category::where('type', 'category')->where('parent_id', null)->get(),
            'tags' => Category::where('type', 'tag')->get(),
            'active' => $post->title ? $post->title : 'Новая запись'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->fill($request->all());

        $post->categories()->sync($request->categories);
        $post->save();

        if(isset($request->image)){
            if($post->miniature()) {
                ImageController::deleteImage($post->miniature()->id);
            }
            ImageController::uploadImage('miniature', 'post', $id, $request->image);
        }

        if(isset($request->gallery)){
            ImageController::massiveUpload($request, 'gallery', 'gallery','post', $id);
        }

        if(isset($request->img_delete)){
            ImageController::massiveDelete($request->img_delete);
        }

        return redirect(route('posts.edit', $post->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $type = $post->type;
        if ($post->trashed()) {
            $post->forceDelete();
            return redirect(route('posts.index', ['type' => $type]));
        } else {
            $post->delete();
            return Redirect::back();
        }
    }

    /**
     * Restore deleted post.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->restore();
        return Redirect::back();
    }
}
