<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * Display a dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return redirect('/admin/orders');
        //return view('admin.pages.dashboard');
    }
}
