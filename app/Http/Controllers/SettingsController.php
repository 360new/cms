<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::first();
        $sizes = [];
        $sizes['thumbnail'] = explode('x', $settings->thumbnail_size);
        $sizes['small']  = explode('x', $settings->small_size);
        $sizes['medium']  = explode('x', $settings->medium_size);
        $sizes['large']  = explode('x', $settings->large_size);

        return view('admin.pages.settings.form', [
            'active'    => 'Настройки',
            'title' => ' | Настройки сайта',
            'settings'  => $settings,
            'sizes' => $sizes
        ]);
    }

    public function update(Request $request)
    {
        $settings = Settings::first();
        $settings->fill($request->all());
        foreach ($request->sizes as $name => $val) {
            ${$name} = $val[0]. 'x' . $val[1];
        }
        $settings->thumbnail_size = $thumbnail;
        $settings->small_size = $small;
        $settings->medium_size = $medium;
        $settings->large_size = $large;
        $settings->save();
        return redirect(route('settings.index'));
    }
}
