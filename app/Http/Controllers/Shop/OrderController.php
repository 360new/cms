<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Helpers\Mail;
use App\Models\Settings;
use App\Models\Shop\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('trashed')) {
            $orders = Order::onlyTrashed();
        } else {
            $orders = Order::whereNotNull('content');
        }
        $orders->whereNotIn('status',['cart']);

        if(isset($request->status)) {
            $orders->where('status', $request->status);
        }
        return view('admin.shop.pages.orders.list', [
            'orders' => $orders->orderBy('created_at', 'desc')->get(),
            'title' => '| Список заказов',
            'active' => 'Заказы'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::withTrashed()->where('id', $id)->first();
        $status = $order->status;
        if ($order->trashed()) {
            $order->forceDelete();
            return redirect(route('orders.index', ['status' => $status]));
        } else {
            $order->delete();
            return Redirect::back();
        }
    }

    /**
     * Restore deleted Order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $order = Order::withTrashed()->where('id', $id)->first();
        $order->restore();
        return Redirect::back();
    }

    /**
     * Update order status
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {
        $order = Order::withTrashed()->where('id', $request->id)->first();
        $order->status = $request->status;
        if($order->save()) {
            return response('success');
        }  else {
            return response('error');
        }
    }
    
    //------------------- Front Methods -------------------\\
    /**
     * Create empty order for storing user cart
     *
     * @return int id
     */
    public function createCartOrder()
    {
        $order = new Order();
        $order->status = 'cart';
        $order->content = '[]';
        $order->user_id = Auth::check()? Auth::user()->id: null;
        $order->save();
        return $order->id;
    }

    /**
     * Create empty order for storing user cart
     *
     * @param  \Illuminate\Http\Request $request
     * @return string cart
     */
    public function getCartOrder(Request $request)
    {
        $order = Order::find($request->order_id);

        return $order->getOrderStat();
    }

    public function getCartProducts(Request $request)
    {
        $order = Order::find($request->order_id);
        return $order->getOrderItems();
    }

    public function updateOrderItems(Request $request)
    {
        $order = Order::find($request->order_id);
        if ($order != null) {
            $order->updateItems($request->product);
            $order->save();
        }
    }

    public function confirmOrder(Request $request)
    {
        $settings = Settings::first();
        $order = isset($_COOKIE['order_id'])? Order::find($_COOKIE['order_id']) : new Order();
        $order->status = 'new';
        $order->fill($request->all());

        //Фиксируем сумму заказа, для подсчёта общей суммы заказов клиента без привязки к стоимости товаров
        $order->summ = $order->getOrderStat()['summ'];

        //Инкрементим счётчик покупок продуктов
        foreach ($order->getOrderItems() as $product) {
            $product->sales += $product->quantity;
            //Убираем информационные поля для возможности сохранения
            unset($product->quantity);
            unset($product->sum);
            $product->save();
        }
        $order->created_at = date('Y-m-d H:i:s');
        $order->number = Order::max('number') + 1;
        //Рассылка уведомлений
        $mail = new Mail("no-reply@2krendelya.ru");
        $mail->setFromName($settings->site_name);
        $mail->send($settings->contacts_email, "Новый заказ на сайте " . $settings->site_name, $this->makeOrderMail($order, true));
        if(!empty($order->email)) {
            $mail->send($order->email, "Ваш заказ на сайте " . $settings->site_name . "принят в обработку", $this->makeOrderMail($order));
        }

        if($order->save()) {
            $cookie = Cookie::forget('order_id');
            return response(view('themes.' . env('THEME') . '.pages.shop.thanks'))->withCookie($cookie);
        }
    }


    public function makeOrderMail(Order $order, $is_admin = false) {
        if($is_admin) {
            $text =  $order->created_at . " поступил новый заказ №" . $order->number . ". Ожидаемое время доставки: " . $order->time . "<br>";
        } else {
            $text = $order->client . ", ваш заказ подтвержден и ему присвоен номер <strong>#" . $order->number . "</strong>. Спасибо за заказ!";
        }

        $text .= "<h2>Содержимое заказа</h2>
        <table border='1' cellpadding='5' cellspacing='0'>
            <tr>
                
                    <th>Позиция</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Сумма</th>
            </tr>";
        foreach ($order->getOrderItems() as $product) {
            $text .= "
                <tr>
                    <td>{$product->name} {$product->weight}</td>
                    <td>{$product->quantity}</td>
                    <td>{$product->price} ₽</td>
                    <td>{$product->sum} ₽</td>
                </tr>
                ";
        }
        $text .= "
            <tr>
                <td colspan='3'>Количество персон: <strong>{$order->persons}</strong></td>
                <td>Итого: <strong>{$order->getOrderStat()['summ']} ₽</strong></td>
            </tr>
        </table>";
        return $text;
    }
}
