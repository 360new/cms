<?php
if (!function_exists('buildCategoriesTree')) {
    function buildCategoriesTree($categories, $object)
    {
        echo '<ul class="ml-15">';
        foreach ($categories as $category) {
            $checked = '';
            if ($object->categories->contains($category->id)) {
                $checked = 'checked';
            }
            echo "<li><input type='checkbox' name='categories[]' value='{$category->id}' " . $checked . " form='form-data'>&nbsp;{$category->name}</li>";
            if ($category->children) {
                buildCategoriesTree($category->children, $object);
            }
        }
        echo '</ul>';
    }
}
if (!function_exists('buildMenuItemsTree')) {
    function buildMenuItemsTree($menus)
    {
        echo "<ol class=\"dd-list\">";
        foreach ($menus as $menu) {

            echo(
                "<li class='dd-item' data-id=\"" . $menu->id . "\">
                <div class=\"dd-handle\">" . $menu->caption . "</div>");
            if (!$menu->children->isEmpty()) {
                buildMenuItemsTree($menu->children);
            }
            echo "</li>";
        }
        echo "</ol>";
    }
}

if (!function_exists('buildCategoriesList')) {
    function buildCategoriesList($categories, $entity = null, $iteration = 1)
    {

        foreach ($categories as $category) {
            if(!$category->parent) {
                $iteration = 1;
            }
            $padding = 15 * $iteration;
            $route = route($entity .'.edit', $category->id);
            $rest_btn  = '';

            if(!is_null($category->deleted_at)) {
                $rest_btn =
                    "<a href=\"#\" data-toggle=\"tooltip\" class=\"mr-10\" data-original-title=\"Восстановить\">
                        <i class=\"fa fa-arrow-up btn-modal-restore text-success\"></i>
                    </a>";
            }
            echo
                "<tr data-id='{$category->id}'>",
                "<td style='padding-left:{$padding}px'>",
                    !($category->children->isEmpty()) || is_null($category->parent_id) ? '<strong>' : null,
                        "<a href='{$route}'>",
                            $category->parent ? "&mdash; " : ' ', $category->name,
                        "</a>",
                    !($category->children->isEmpty()) || is_null($category->parent_id) ? '</strong>' : '',
                "</td>",
                "<td>{$category->created_at}</td>",
                "<td>{$category->user->name}</td>",
                "<td class=\"text-nowrap\">",
                    "<a href=\"{$route}\" class=\"mr-10\" data-toggle=\"tooltip\" data-original-title=\"Редактировать\">",
                        "<i class=\"fa fa-pencil text-inverse\"></i>",
                    "</a>",
                    "<a href=\"#\" data-toggle=\"tooltip\" class=\"mr-10\" data-original-title=\"Удалить\">",
                        "<i class=\"fa fa-close btn-modal-close text-danger\"></i>",
                    "</a>",
                    $rest_btn,
            "</tr>";

            if(!$category->children->isEmpty()) {
                buildCategoriesList($category->children, $entity, ++$iteration);
            }
        }


    }
}