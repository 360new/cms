<?php

namespace App\Helpers;

use App\Models\Post;
use Illuminate\Http\Request;

class FrontRenderHelper {

    public static function getCategoryPosts($category, $quantity)
    {
        return Post::whereHas('categories', function($query) use ($category) {
            $query->where('slug', 'LIKE', "$category");

        })->take($quantity)->get();
    }
}