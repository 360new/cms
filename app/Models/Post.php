<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['status', 'type', 'title', 'slug', 'content', 'additional', 'user_id', 'description', 'keywords'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'parent');
    }

    public function miniature()
    {
        return $this->images()->where('type', 'miniature')->first();
    }

    public function gallery()
    {
        return $this->images()->where('type', 'gallery')->get();
    }
}
