<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = ['type', 'status', 'name', 'slug', 'caption', 'parent_id', 'user_id', 'description', 'keywords'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'parent');
    }

    public function miniature()
    {
        return $this->images()->where('type', 'miniature')->first();
    }

    public function gallery()
    {
        return $this->images()->where('type', 'gallery')->get();
    }
    public function getReadableStatus()
    {
        $status = null;
        if (!is_null($this->deleted_at)) {
            $status = 'В корзине';
        } else {
            switch ($this->status) {
                case 'hidden':
                    $status = 'Черновик';
                    break;
                case 'published':
                    $status = 'Опубликовано';
                    break;
            }
        }
        return $status;
    }

    public function getUrl()
    {
        $url = $this->slug;

        $category = $this;

        while ($category = $category->parent) {
            $url = $category->slug.'/'.$url;
        }

        return '/pages/'.$url;
    }

}
