<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['site_name', 'code', 'contacts_email', 'default_description', 'default_keywords', 'fit_thumbnail'];
}
