<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use stdClass;

class Order extends Model
{

    use SoftDeletes;

    protected $fillable = ['client', 'email', 'phone', 'address', 'payment', 'time', 'persons'];

    public const STATUSES = [
        "new" => "Новый",
        "proceed" => "В обработке",
        "collecting" => "Готовится",
        "finished" => "Завершен",
        "unconfirmed" => "Не подтвержден",
        "cart" => 'Корзина клиента'
    ];

    public const  PAYMENTS = [
        'card' => 'Картой',
        'cash' => 'Наличные',
        'invoice' => 'Безналичный расчёт'
    ];


    public function getReadableContent()
    {

        $string = '';
        $order_content = json_decode($this->content);
        if (!is_null($order_content)) {

            foreach ($order_content as $prod) {
                $product = Product::find($prod->id);
                $string .= $product->name . ': ' . $prod->quantity . 'шт. | ';
            }
        }
        return $string;
    }


    public function getOrderStat()
    {
        $summ = 0;
        $qnt = 0;
        if ($this->content == '') {
            return 0;
        }

        $order_content = json_decode($this->content);
        foreach ($order_content as $prod) {
            $product = Product::find($prod->id);
            $summ += $product->price * $prod->quantity;
            $qnt += $prod->quantity;

        }
        return ['summ' => round($summ, 2), 'quantity' => $qnt];
    }

    public function getOrderItems()
    {
        $products = collect();
        $order_content = json_decode($this->content);

        foreach ($order_content as $prod) {
            $product = Product::where('id', $prod->id)->with(['images' => function ($query) {
                $query->where('type', 'miniature');
            }])->first();
            $product->quantity = $prod->quantity;
            $product->sum = $product->price * $prod->quantity;
            $products->push($product);
        }
        return $products;
    }

    public function updateItems($product)
    {
        $need_add = true;
        $unset = null;
        $order_content = ((array)json_decode($this->content));
        if($order_content == null) {
            $order_content = [];
        }

        //Перебираем заказ и проверяем есть ли в нём переданный элемент
        foreach ((array)$order_content as $key => $item) {
            if ($item->id == (int)$product['id']) {
                //Если элемент уже есть, то не создаем новый
                $need_add = false;
                if ($product['quantity'] == 0) {
                    $unset = $key;
                } else {
                    $item->quantity = $product['quantity'];
                }
            }
        }
        //Добавляем новый элемент в заказ
        if ($need_add && $product['quantity'] != 0) {
            $object = new stdClass();
            $object->id = $product['id'];
            $object->quantity = $product['quantity'];
            $order_content [] = $object;
        }
        //Удаляем элемент
        if (!is_null($unset)) {
            unset($order_content[$unset]);
        }
        $this->content = json_encode(array_values($order_content));
    }

}
