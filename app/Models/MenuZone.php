<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuZone extends Model
{
    public function menus() {
        return $this->hasMany('App\Models\Menu', 'menu_zone');
    }

    public function baseItems()
    {
        return $this->menus->where('parent_id', null);
    }

}
