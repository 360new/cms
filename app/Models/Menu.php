<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    public function scopeParentles($query)
    {
        return $query->where('parent_id', null);
    }


    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id', 'id');
    }


}
