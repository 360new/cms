<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contacts_email')->nullable();
            $table->string('site_name')->nullable();
            $table->string('default_keywords')->nullable();
            $table->text('default_description')->nullable();
            $table->text('code')->nullable();
            $table->boolean('fit_thumbnail')->nullable();
            $table->string('thumbnail_size')->default('150x150');
            $table->string('small_size')->default('300x300');
            $table->string('medium_size')->default('600x600');
            $table->string('large_size')->default('1024x1024');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
