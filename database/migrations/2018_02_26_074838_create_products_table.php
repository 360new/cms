<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('public');
            $table->string('article')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->double('price')->nullable();
            $table->string('weight')->nullable();
            $table->boolean('top')->nullable();
            $table->boolean('sold')->nullable();
            $table->longText('short_product_description')->nullable();
            $table->longText('additional_product_description')->nullable();
            $table->longText('product_description')->nullable();
            $table->text('description')->nullable();
            $table->string('keywords')->nullable();
            $table->unsignedInteger('minimum')->default(0);
            $table->unsignedInteger('sales')->default(0);
            $table->string('template')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
