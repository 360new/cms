<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->nullable();
            $table->string('client')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('time')->nullable();
            $table->string('payment')->default('cash');
            $table->text('address')->nullable();
            $table->text('notes')->nullable();
            $table->integer('persons')->nullable();
            $table->mediumText('content');
            $table->string('status')->nullable();
            $table->double('summ')->nullable();
            $table->unsignedInteger('user_id')->default(1)->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
