<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Superadmin',
            'email' => 'dev.vladimirkarpenko@gmail.com',
            'password' => bcrypt('karpenko'),
            'role_id' => 1
        ]);

        $this->call(SettingsTableSeeder::class);

    }
}
